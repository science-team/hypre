#!/usr/bin/make -f
# Made with the aid of debmake, by Christoph Lameter,
# based on the sample debian/rules file for GNU hello by Ian Jackson.

include /usr/share/dpkg/default.mk

DEB_VERSION := $(shell dpkg-parsechangelog -S Version)
DEB_NOEPOCH_VERSION := $(shell echo $(DEB_VERSION) | cut -d: -f2-)
DEB_UPSTREAM_VERSION := $(shell echo $(DEB_NOEPOCH_VERSION) | sed 's/-[^-]*$$//')

HYPRE_UPSTREAM_VERSION=$(DEB_UPSTREAM_VERSION)
HYPRE_X_Y_VERSION=$(shell echo $(HYPRE_UPSTREAM_VERSION) | sed "s/^\(.*\)\.\([^.]*\)$$/\1/")
HYPRE_Z_VERSION=$(shell echo $(HYPRE_UPSTREAM_VERSION) | sed "s/^\(.*\)\.\([^.]*\)$$/\2/")
HYPRE_NEXT_VERSION=$(HYPRE_X_Y_VERSION).$(shell echo $$(( $(HYPRE_Z_VERSION) + 1 )) )

export HYPRE_SOVERSION=$(HYPRE_UPSTREAM_VERSION)

export DEB_CFLAGS_MAINT_APPEND  = -Wall -pedantic -O3
export DEB_CXXFLAGS_MAINT_APPEND  = -Wall -pedantic -O3
export DEB_LDFLAGS_MAINT_APPEND = -Wl,--no-as-needed
export DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)
export DEB_HOST_ARCH ?= $(shell dpkg-architecture -qDEB_HOST_ARCH)

%:
	dh $@ --sourcedirectory=src

# to avoid lintian warnings
CPPFLAGS:=$(shell dpkg-buildflags --get CPPFLAGS)
CFLAGS:=$(shell dpkg-buildflags --get CFLAGS)
CXXFLAGS:=$(shell dpkg-buildflags --get CXXFLAGS)
LDFLAGS:=$(shell dpkg-buildflags --get LDFLAGS)

# 64-bit build is not supported on all arches
ifneq (64,$(DEB_HOST_ARCH_BITS))
export DH_OPTIONS += \
	-Nlibhypre64-dev -Nlibhypre64-${HYPRE_SOVERSION} \
	-Nlibhypre64m-dev -Nlibhypre64m-${HYPRE_SOVERSION}
endif

# extra flags set to be similar to what petsc requires
extra_flags_common += --with-superlu --with-superlu-include=/usr/include/superlu --with-superlu-lib="-lsuperlu" \
  --with-dsuperlu --with-dsuperlu-include=/usr/include/superlu-dist --with-dsuperlu-lib="-lsuperlu_dist" \
  --with-MPI-include=$(shell pkg-config --cflags-only-I mpi | awk '{print $$1}' | sed "s/^-I//") \
  --with-MPI-libs="$(shell pkg-config --libs-only-l mpi | sed 's/-l//g')" \
  --with-MPI-lib-dirs="$(shell pkg-config --libs-only-L mpi | sed 's/-L//g')" \
  --with-MPI-flags="$(shell pkg-config --libs-only-other mpi)" \
  --with-extra-flags="$(shell pkg-config --cflags-only-other mpi)"

# from pets:
# # explicitly tell hypre BLAS/LAPACK mangling since it may not match Fortran mangling
#    if self.blasLapack.mangling == 'underscore':
#      mang = 'one-underscore'
#    elif self.blasLapack.mangling == 'caps':
#      mang = 'caps-no-underscores'
#    else:
#      mang = 'no-underscores'
#    args.append('--with-fmangle-blas='+mang)
#    args.append('--with-fmangle-lapack='+mang)


# Other options from src/configure
  # --enable-debug          Set compiler flags for debugging.
  # --enable-shared         Build shared libraries (default is NO).
  # --enable-bigint         Use long long int for HYPRE_Int (default is NO).
  # --enable-complex        Use complex values (default is NO).
  # --enable-maxdim=MAXDIM  Change max dimension size to MAXDIM (default is 3).
  #                         Currently must be at least 3.
  # --enable-persistent     Uses persistent communication (default is NO).
  # --enable-hopscotch      Uses hopscotch hashing if configured with OpenMP and
  #                         atomic capability available(default is NO).
  # --enable-global-partition
  #                         Use global partitioning (default is NO).
  # --enable-fortran        Require a working Fortran compiler (default is YES).

export PRTE_MCA_plm_ssh_agent=/bin/false
export PRTE_MCA_rmaps_default_mapping_policy=:oversubscribe
export OMPI_MCA_btl_tcp_if_include=lo

execute_after_dh_clean:
	rm -rf src64 src64m
	rm -rf src-static src64-static src64m-static

override_dh_auto_clean:
	echo "Running clean"
	rm -rf $(CURDIR)/debian/tmp
	rm -f src/configure.in
	rm -f src/config.log
	rm -f src/config/config.guess
	rm -f src/config/config.sub
	cd src/test; \
	for TEST in TEST_*; do \
	  rm -f $${TEST}.stdout $${TEST}.stderr; \
	done
	-dh_auto_clean

override_dh_autoreconf:
	cp /usr/share/misc/config.* src/config/
	cd src && ./config/update.sh
	rm -rf src64 && cp -a src src64
	rm -rf src64m && cp -a src src64m
	rm -rf src-static && cp -a src src-static
	rm -rf src64-static && cp -a src src64-static
	rm -rf src64m-static && cp -a src src64m-static


override_dh_auto_configure:
	dh_auto_configure -- --enable-shared $(extra_flags_common) \
	  --with-blas=yes --with-blas-lib="$(shell pkg-config --libs blas)" \
	  --with-lapack=yes \
	    --with-lapack-lib="$(shell pkg-config --libs lapack)" \
	    --with-fmangle-lapack="one-underscore"   \
	  --prefix=$(CURDIR)/debian/tmp/usr
	dh_auto_configure -Dsrc-static -- $(extra_flags_common) \
	  --with-blas=yes --with-blas-lib="$(shell pkg-config --libs blas)" \
	  --with-lapack=yes \
	    --with-lapack-lib="$(shell pkg-config --libs lapack)" \
	    --with-fmangle-lapack="one-underscore"   \
	  --prefix=$(CURDIR)/debian/tmp-static/usr
ifeq (64,$(DEB_HOST_ARCH_BITS))
	dh_auto_configure -Dsrc64 -- --enable-bigint --with-libname-ext=64 \
	  --enable-shared $(extra_flags_common) \
	  --with-blas=yes --with-blas-lib="$(shell pkg-config --libs blas64)" \
	  --with-lapack=yes \
	    --with-lapack-lib="$(shell pkg-config --libs lapack64)" \
	    --with-fmangle-lapack="one-underscore"   \
	  --prefix=$(CURDIR)/debian/tmp64/usr
	dh_auto_configure -Dsrc64m -- --enable-mixedint --with-libname-ext=64m \
	  --enable-shared $(extra_flags_common) \
	  --with-blas=yes --with-blas-lib="$(shell pkg-config --libs blas64)" \
	  --with-lapack=yes \
	    --with-lapack-lib="$(shell pkg-config --libs lapack64)" \
	    --with-fmangle-lapack="one-underscore"   \
	  --prefix=$(CURDIR)/debian/tmp64m/usr
	dh_auto_configure -Dsrc64-static -- --enable-bigint --with-libname-ext=64 \
	  $(extra_flags_common) \
	  --with-blas=yes --with-blas-lib="$(shell pkg-config --libs blas64)" \
	  --with-lapack=yes \
	    --with-lapack-lib="$(shell pkg-config --libs lapack64)" \
	    --with-fmangle-lapack="one-underscore"   \
	  --prefix=$(CURDIR)/debian/tmp64-static/usr
	dh_auto_configure -Dsrc64m-static -- --enable-mixedint --with-libname-ext=64m \
	  $(extra_flags_common) \
	  --with-blas=yes --with-blas-lib="$(shell pkg-config --libs blas64)" \
	  --with-lapack=yes \
	    --with-lapack-lib="$(shell pkg-config --libs lapack64)" \
	    --with-fmangle-lapack="one-underscore"   \
	  --prefix=$(CURDIR)/debian/tmp64m-static/usr
	#dh_auto_configure -p libhypre-complex -- $(extra_flags) --enable-shared --enable-complex
endif

override_dh_auto_build:
	dh_auto_build -Dsrc
	dh_auto_build -Dsrc-static
ifeq (64,$(DEB_HOST_ARCH_BITS))
	dh_auto_build -Dsrc64
	dh_auto_build -Dsrc64m
	dh_auto_build -Dsrc64-static
	dh_auto_build -Dsrc64m-static
endif

override_dh_auto_install:
	dh_auto_install -Dsrc
	dh_auto_install -Dsrc-static
ifeq (64,$(DEB_HOST_ARCH_BITS))
	dh_auto_install -Dsrc64
	dh_auto_install -Dsrc64m
	dh_auto_install -Dsrc64-static
	dh_auto_install -Dsrc64m-static
endif

# don't run tests if the nocheck build option has been specified
RUNTEST=yes
ifeq (nocheck,$(findstring nocheck,$(DEB_BUILD_OPTIONS)))
  RUNTEST=no
endif

empty :=
space := $(empty)$(empty)

# ia64 does not fail to run tests, it fails to build them
ARCH_NO_TEST_LIST = ia64

ifneq (,$(findstring $(space)$(DEB_HOST_ARCH)$(space), $(space)$(ARCH_NO_TEST_LIST)$(space)))
  RUNTEST=no
endif

# some arches consistently hang in checkpar tests
# possibly due to pmix, see hypre upstream issue #955
ARCH_NO_CHECKPAR_LIST = ppc64el hurd-i386

ifneq (,$(findstring $(space)$(DEB_HOST_ARCH)$(space), $(space)$(ARCH_NO_CHECKPAR_LIST)$(space)))
  RUN_CHECKPAR=no
endif


# Standard tolerance 1e-6 is too tight on many arches
# so relax them to 1e-3
ARCH_RELAX_TOL_LIST = arm64 i386 ppc64el riscv64 s390x hppa loong64 powerpc ppc64

ifneq (,$(findstring $(space)$(DEB_HOST_ARCH)$(space), $(space)$(ARCH_RELAX_TOL_LIST)$(space)))
  RELAX_TOL = HYPRE_CHECKPAR_TOL=1e-3
endif

ifeq (64,$(DEB_HOST_ARCH_BITS))
TEST_SRC_DIRS = src src-static src64 src64-static
else
TEST_SRC_DIRS = src src-static
endif

override_dh_auto_test:
	set -e; \
	  echo "max available processors nproc=`nproc`"; \
	if [ "$(RUNTEST)" = "no" ]; then \
	  echo Tests have been disabled on $(DEB_HOST_ARCH); \
	else echo "=== Building tests ==="; \
	  for build in $(TEST_SRC_DIRS); do \
	    echo "=== Running tests from $$build ==="; \
	    cd $$build; \
	    TEST_LD_LIBRARY_PATH=$(CURDIR)/$$build/lib:$(LD_LIBRARY_PATH); \
	    echo "== running make check (single) =="; \
	    LD_LIBRARY_PATH=$${TEST_LD_LIBRARY_PATH} make check; \
	    echo "== running make check (MPI -P 3 1 1) =="; \
	    LD_LIBRARY_PATH=$${TEST_LD_LIBRARY_PATH} CHECKRUN="mpirun -n 3" PARMS='-P 3 1 1' make check; \
	    if [ "x$(RUN_CHECKPAR)" = "xno" ]; then \
	      echo "== skipping make checkpar on $(DEB_HOST_ARCH) =="; \
	    else \
	      echo "== running make checkpar =="; \
	      LD_LIBRARY_PATH=$${TEST_LD_LIBRARY_PATH} make checkpar $(RELAX_TOL); \
	    fi; \
	    cd ..; \
	  done; \
	fi

# upstream places the soname in front of .so
# which makes it trickier than desirable to exclude
# the versioned so files in a libhypre-dev.install file
# Easier to just override instead
override_dh_install:
	dh_install -p libhypre-${HYPRE_SOVERSION} usr/lib/*/*${HYPRE_SOVERSION}.so
	dh_install -p libhypre-dev --exclude=${HYPRE_SOVERSION} usr/lib/*/*
	dh_install -p libhypre-dev usr/include/hypre
	dh_install -p libhypre-dev --sourcedir=debian/tmp-static usr/lib/*/libHYPRE.a
ifeq (64,$(DEB_HOST_ARCH_BITS))
	dh_install --sourcedir=debian/tmp64 -p libhypre64-${HYPRE_SOVERSION} usr/lib/*/*${HYPRE_SOVERSION}.so
	dh_install --sourcedir=debian/tmp64 -p libhypre64-dev --exclude=${HYPRE_SOVERSION} usr/lib/*/*
	dh_install --sourcedir=debian/tmp64 -p libhypre64-dev usr/include/hypre/* usr/include/hypre64
	dh_install --sourcedir=debian/tmp64-static -p libhypre64-dev usr/lib/*/libHYPRE64.a
	dh_install --sourcedir=debian/tmp64m -p libhypre64m-${HYPRE_SOVERSION} usr/lib/*/*${HYPRE_SOVERSION}.so
	dh_install --sourcedir=debian/tmp64m -p libhypre64m-dev --exclude=${HYPRE_SOVERSION} usr/lib/*/*
	dh_install --sourcedir=debian/tmp64m -p libhypre64m-dev usr/include/hypre/* usr/include/hypre64m
	dh_install --sourcedir=debian/tmp64m-static -p libhypre64m-dev usr/lib/*/libHYPRE64m.a
endif
